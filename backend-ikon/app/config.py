import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = "postgres://postgres:heaven@localhost:5432/ikonApp"
    SECRET_KEY = '@icons@@U%J&36$##$ghjg%^%&*^^&%^$#%^&'
