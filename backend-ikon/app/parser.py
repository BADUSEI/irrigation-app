from flask_restful import reqparse

userParser = reqparse.RequestParser()
userParser.add_argument(
    'fname',
    type=str,
    required=True,
    help="This field can't be blank."
)

userParser.add_argument(
    'mname',
    type=str,
    required=True,
    help="This field can't be blank."
)

userParser.add_argument(
    'lname',
    type=str,
    required=True,
    help="This field can't be blank."
)

userParser.add_argument(
    'phone',
    type=str,
    required=True,
    help="This field can't be blank."
)

userParser.add_argument(
    'email',
    type=str,
    required=True,
    help="This field can't be blank."
)

userParser.add_argument(
    'password',
    type=str,
    required=True,
    help="This field can't be blank."
)

userParser.add_argument(
    'app_token',
    type=str,
    required=False,
    help="This field can't be empty."
)

userParser.add_argument(
    'token',
    type=str,
    required=False,
    help="This field isn't required."
)


linkParser = reqparse.RequestParser()
linkParser.add_argument(
    'email',
    type=str,
    required=True,
    help="This field can't be blank."
)


accountParser = reqparse.RequestParser()
accountParser.add_argument(
    'email',
    type=str,
    required=True,
    help="This field can't be blank."
)

accountParser.add_argument(
    'password',
    type=str,
    required=True,
    help="This field can't be blank."
)
# for recovery validation
accountParser.add_argument(
    'token',
    type=str,
    required=False,
    help="This field is set to default."
)