import 'package:flutter/material.dart';
import './src/screens/login.dart';
import './src/resources/fetch_data.dart';
import './src/screens/home.dart';
import './src/screens/signup.dart';
import './src/screens/password_retrieval.dart.dart';
import './src/screens/reset_password.dart';


//Entrypoint
void main() => runApp(App());

class App extends StatelessWidget {
  // This widget is the root of the application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Login(),  
      debugShowCheckedModeBanner: false,

      //Routings
      routes: <String, WidgetBuilder> {
        "/Login":(context) =>Login(),
        "/Home" : (context) => Home(),
        "/SignUp" : (context) => SignUp(),
        "/retrieve" : (context) => RetrievePassword(),
        "/reset" : (context) => ResetPassword(),
        "/FetchContact" : (context) => FetchContact(),
      },
    );
  }
}


