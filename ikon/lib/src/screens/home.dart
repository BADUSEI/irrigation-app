import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../resources/services.dart';

class Home extends StatefulWidget{
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  SharedPreferences sharedPreferences; // variable for login token operations
  Color active = Colors.lightGreenAccent[400];
  Color inactive = Colors.amberAccent[400];
  bool activeColor1 = false;
  bool activeColor2 = false;
  bool activeColor3 = false;

  // checks the state in a non-stop manner
  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  // consistently checks if login access token is available and takes respective action
  checkLoginStatus() async{
    sharedPreferences = await SharedPreferences.getInstance();
    if(sharedPreferences.getString("access_token") == null){ 
      Navigator.of(context).pushNamed("/Login");  
    } else {
      _showToast();
    }
  }

  void _showToast() {
    Fluttertoast.showToast(
      msg: 'Successfully Logged in',
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      //timeInSecForIos: 1,
      backgroundColor: Colors.black12,
      textColor: Colors.black
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        centerTitle: false,
        title: Text('Home'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            iconSize: 26, 
            onPressed: ()async{
              sharedPreferences = await SharedPreferences.getInstance();
              sharedPreferences?.clear(); 
              Navigator.of(context).pushNamed("/Login");
            },
          ),
        ],
      ),
      body: _homePage(context), 
    );
  }

  // Quick Access buttons
  Widget _homePage(BuildContext context) {
    return ListView(
        children: <Widget> [
          _MyCarousel(),
          Card(
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(0, 15,10,8 ),
                  child: Text(
                    'Quick Control',
                    style: TextStyle(color: Colors.black, fontSize:25)
                  ),
                ),
                Divider(),
                Row(
                 mainAxisAlignment: MainAxisAlignment.spaceAround,
                 children: <Widget>[
                   Column(
                     children: <Widget>[
                       SizedBox(
                         height: 72,
                         width: 72,
                         child: RaisedButton(
                          onPressed: () {
                            Connections().getPump(); //linking arduino
                            setState(() {
                              activeColor1 = true;
                              activeColor2 = false;
                              activeColor3 = false;
                            });
                          },
                          color: activeColor1? active: inactive,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),
                        ),
                       ),
                       SizedBox(height:5),
                       Text(
                         "Pump Water",
                          style: TextStyle(fontSize:12, fontWeight: FontWeight.w600),
                       )
                     ]
                   ),
                   Column(
                     children: <Widget>[
                       SizedBox(
                         height: 72,
                         width: 72,
                         child: RaisedButton(
                          onPressed: () {
                            Connections().getIrrigate(); //linking arduino
                            setState(() {
                              activeColor2 = true;
                              activeColor1 = false;
                              activeColor3 = false;
                            });
                          },
                          color: activeColor2? active: inactive,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),
                        ),
                       ),
                       SizedBox(height:5),
                       Text(
                         "Irrigate Site",
                          style: TextStyle(fontSize:12, fontWeight: FontWeight.w600),
                       )
                     ]
                   ),
                ],
              ),
              SizedBox(height: 40),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(
                     children: <Widget>[
                       SizedBox(
                         height: 72,
                         width: 72,
                         child: RaisedButton(
                          onPressed: () {
                            Connections().getStop(); //linking arduino
                            setState(() {
                              activeColor3 = true;
                              activeColor2 = false;
                              activeColor1 = false;
                            });
                          },
                          color: activeColor3? active: inactive,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),
                        ),
                       ),
                       SizedBox(height:5),
                       Text(
                         "Stop Pumping/Irrigate",
                          style: TextStyle(fontSize:12, fontWeight: FontWeight.w600),
                       )
                     ]
                   ),
                   Column(
                     children: <Widget>[
                       SizedBox(
                         height: 72,
                         width: 72,
                         child: RaisedButton(
                          onPressed: () {
                            Navigator.pushNamed(context, "/FetchContact");
                          },
                          splashColor: Colors.red,
                          child: Icon(
                             Icons.feedback,
                             color: Colors.red,
                             size: 44.0,
                          ),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(50.0))),
                          color: Colors.white,
                        ),
                       ),
                       SizedBox(height:5),
                       Text(
                         "Notifications",
                          style: TextStyle(fontSize:12, fontWeight: FontWeight.w600),
                       )
                     ]
                   ),
                  ],
                ),
                Text('')
              ]
            ),
          ),
        ]
      );
  }
}


class _MyCarousel extends StatelessWidget{
  final carousel = Carousel(
    boxFit: BoxFit.fill,
    animationCurve: Curves.fastOutSlowIn,
    autoplayDuration: Duration(milliseconds: 5000),
    images: [
      AssetImage('assets/images/bkg.jpeg'),
      AssetImage('assets/images/irrig.jpg'),
    ],
  );

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Container(
      child: Container(
        padding: EdgeInsets.all(5),
        height: (screenHeight*3)/8,
        child: ClipRRect(
          // borderRadius: BorderRadius.circular(15),
          child: Stack(
            children: <Widget>[
              carousel,
              // Banner(),
            ]
          )
      )
    ));
  }
}

class Banner extends StatefulWidget{
  @override
  _BannerState createState() => _BannerState();
}

class _BannerState extends State<Banner> with SingleTickerProviderStateMixin{

  AnimationController controller;
  Animation<double> animation;

  initState() {
    super.initState();
    controller = AnimationController(
      duration: Duration(milliseconds: 5000),
      vsync: this,
    );
    animation = Tween(begin: 0.0, end: 18.0).animate(controller)
      ..addListener((){
      controller.forward();
    });
  }

  @override
  Widget build(BuildContext context){
      return Padding(
        padding: const EdgeInsets.only(left:10, top:10),
        child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.amber.withOpacity(0.5),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              bottomRight: Radius.circular(15)
            )
          ),
          child: Text(
            'Watertank App features',
            style: TextStyle(
              fontFamily: 'fira',
              fontSize: 18
            ),
          ),
        ),
    );
  }

  dispose() {
    controller.dispose();
    super.dispose();
  }
}
