import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';
import '../validation_mixin.dart';

class SignUp extends StatefulWidget{
  @override
  _SignUpState createState ()=> _SignUpState();

}

class _SignUpState extends State<SignUp> with ValidationMixin{

  final formkey = GlobalKey<FormState>(); //represents the formstate object
  String fname = '';
  String mname = '';
  String lname = '';
  String phoneNumber = '';
  String email = '';
  String password = '';
  String location = '';
  bool _obscureText = true;
  bool _isLoading = false;
  String prompt = '';
  bool alert = false;

  void _showToast() {
    Fluttertoast.showToast(
      msg: prompt,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.CENTER,
      //timeInSecForIos: 1,
      backgroundColor: Color(0xfff5f5f5),
      textColor: Colors.black
    );
  }

  sign_up(fname,mname, lname, phoneNumber, email, password) async{
    Map data = {
      'fname': fname, 
      'mname': mname,
      'lname': lname,
      'phone': phoneNumber,
      'email': email,
      'password': password,
      //'api_key': 'AqwYuidfhfkff.tYnsdbsbs5.9fdhjGhsruk34fjjHYKADVDMdshhhhd2'
    };
    var jsonData;
    http.Response response = await http.post("http://192.168.43.219:5000/apis/mobile/client/signup", body: data);
    if (response.statusCode < 200 || response.statusCode > 400 || json == null) {
        throw Exception("Could'nt connect, please ensure you have a stable network or further chrck ur PC IP adresss.");
    }
    else {
      jsonData = json.decode(response.body);
      print(jsonData);
      String server_message = jsonData['message'];
      if(server_message.contains('Registered successfully')){
        setState(() {
         _isLoading = false;
         alert = false;
         Navigator.pushNamed(context, "/Home"); 
        });
      } else {
        setState(() {
          alert = true;
          prompt = server_message;
          _isLoading = false; 
        });
      }  
    }
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Sign Up'),
        backgroundColor: Colors.red,
        elevation: 0,
        iconTheme:  IconThemeData(color: Colors.white),
      ),
      backgroundColor: Color(0xfff5f5f5),
      body: Center(
        child: _isLoading ? Center(child: CircularProgressIndicator()) : ListView(
          shrinkWrap: true,
          padding: EdgeInsets.all(15),
          children: <Widget>[
            Center(
              child: Form(
                key: formkey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Card(
                      child: Column(
                        children: <Widget>[
                          Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: new TextFormField(
                        keyboardType: TextInputType.text, 
                        decoration: new InputDecoration(
                          prefixIcon: Icon(Icons.person),
                          labelText: 'First Name',
                        ),
                        validator: fieldCheck,
                        onSaved: (String value){
                          fname = value;
                        },                     
                      )
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: TextFormField(
                        keyboardType: TextInputType.text, 
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.person),
                          labelText: 'Middle Name(Optional)',
                          hintText: 'Optional'
                        ),
                        onSaved: (String value){
                          mname = value;
                        },                     
                      )
                    ),
                    SizedBox(height: 8),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child:TextFormField( 
                        keyboardType: TextInputType.text,
                        decoration:InputDecoration(
                          prefixIcon: Icon(Icons.person),
                          labelText: 'Last Name',
                        ),
                        validator: fieldCheck,
                        onSaved: (String value){
                          lname = value;
                        },                     
                      )
                    ),
                    SizedBox(height: 8),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: new TextFormField( 
                        keyboardType: TextInputType.phone,
                        decoration: new InputDecoration(
                          prefixIcon: Icon(Icons.contact_phone),
                          labelText: 'Phone Number',
                          hintText: '0XXXXXXXXX'
                        ),
                        validator: validateContact,
                        onSaved: (String value){
                          phoneNumber = value;
                        },
                      )
                    ),
                    new SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: TextFormField( 
                        autofocus: false,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.contact_mail),
                          labelText: 'Email Address',
                          hintText: 'you@example.com'
                        ),
                        validator: validateEmail,
                        onSaved: (String value){
                          email = value;
                        },
                      ),
                    ),
                    new SizedBox(
                      height: 8,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      child: new TextFormField( 
                        obscureText: _obscureText,
                        decoration: new InputDecoration(
                          suffixIcon: GestureDetector(
                                 child: Icon(
                                   _obscureText ? Icons.visibility : Icons.visibility_off,
                                  ),
                                 onTap: (){
                                   setState(() {
                                     _obscureText = !_obscureText;
                                    });
                                 }
                                ),
                          prefixIcon: Icon(Icons.lock),
                          labelText: 'Password',
                          hintText: 'Password'
                        ),
                        validator: validatePassword,
                        onSaved: (String value){
                          password = value;
                        },
                      )),
                      SizedBox(height: 8),
                    ]
                    ),),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: RaisedButton(
                          color: Colors.red,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                            side: BorderSide(color: Color(0xFF179CDF))
                          ),
                          child: Text(' Submit ', style: TextStyle(fontSize:25.0, color: Colors.white )),
                          onPressed: (){
                            if(formkey.currentState.validate()){
                              formkey.currentState.save();
                              setState(() {
                                _isLoading = true;
                              });
                              sign_up(fname,mname, lname, phoneNumber, email, password);
                              formkey.currentState.reset(); 
                              alert ? _showToast() : prompt= '';  
                              //Navigator.pushNamed(context, "/Home");                         
                            }
                          },
                      )
                    ),
                  ],
                )
              )
            )
          ]
        ) 
      )
    );
  }
}

