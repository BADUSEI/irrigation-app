import 'package:http/http.dart' as http;
import 'dart:convert';


// Ardiuno connnections or linking
class Connections{

  // The below endpoints and IP are are assumed
  final _root = "http://192.168.4.1";  //Arduino IP Address for stop, pump and irrigate...

  getPump() async{
    print('Pumping');
    http.Response response = await http.get("$_root/?State=P"); // put pump endpoint here
    return response.body;
  }

  getIrrigate() async{
    print('Irrigating farm');
    http.Response response = await http.get("$_root/?State=I"); // put irrigate endpoint here
    return response.body;
  }

  getStop() async{
    print('Stop called');
    http.Response response = await http.get("$_root/?State=S"); // put stop endpoint here
    return response.body;
  }

  getMessage() async{
    http.Response response = await http.get("$_root/?State=D"); // put get notifications endpoint here
    var distance = json.decode(response.body);
    print(distance);
    return distance;
  }
}

